import argparse
import datetime
import sys
from pathlib import Path

import polars as pl
from dateutil import parser as datetime_parsers

from services.logging import AppLogger
from services.routing_query import Station, routing_query
from services.station_list_reader import station_list

logger = AppLogger('generate_batchfile').logger

stations = station_list()


def find_station_by_name(station_name: str) -> Station:
    """Find station (public transport stop) by name

    Arguments:
        station_name -- Station name

    Returns:
        Station
    """
    res = stations.filter(pl.col('designationOfficial') == station_name)
    if len(res) == 0:
        sys.exit('Station not found: %s', station_name)
    res = res.row(0, named=True)
    return Station(
        name=res['designationOfficial'],
        identifier=res['number']
    )


def create_motis_batchfile(
    start_stations: list[Station],
    min_altitude_m: float,
    output_filepath: str,
    start_datetime: datetime
):
    """Generate motis query batchfile

    Arguments:
        start_station -- Start station
        min_altitude_m -- Minimum destination altitude
        output_filepath -- Output batchfile
        start_datetime -- Start datetime
    """
    logger.info('Generating motis batchfile')
    destinations = stations.filter(pl.col('height') >= min_altitude_m)
    logger.info(
        '%s destinations found with altitude >= %s m',
        len(destinations),
        min_altitude_m
    )

    with Path.open(output_filepath, "w", encoding='UTF-8') as f:
        for start_station in start_stations:
            logger.info('Processing start [%s]', start_station.name)
            for stn in destinations.iter_rows(named=True):
                destination_station = Station(
                    name=stn['designationOfficial'],
                    identifier=stn['number']
                )
                f.write(
                    routing_query(
                        start_station=start_station,
                        destination_station=destination_station,
                        starts_at=start_datetime
                    ) + '\n'
                )


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Generate motis query list')
    parser.add_argument(
        '-a', '--altitude', default=800.0, type=float, dest='min_altitude_m',
        help='Minimal destination altitude'
    )
    parser.add_argument(
        '-s', '--start_stations', action='append',
        type=str, dest='start_stations', help='Start stations'
    )
    parser.add_argument(
        '-o', '--output_file', default='/data/batch_in.json', type=Path,
        dest='output_file', help='Output file'
    )
    parser.add_argument(
        '-t', '--starts_at', default=datetime.datetime.now(tz=datetime.UTC),
        type=lambda t: datetime_parsers.parse(t),
        dest='starts_at', help='Start datetime'
    )
    args = parser.parse_args()
    stn_list = [find_station_by_name(stn) for stn in args.start_stations]

    create_motis_batchfile(
        start_stations=stn_list,
        min_altitude_m=args.min_altitude_m,
        output_filepath=args.output_file,
        start_datetime=args.starts_at
    )
