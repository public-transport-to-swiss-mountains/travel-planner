#!/bin/bash

# Internal resources
EXTERNAL_RESOURCES_FILE=./config/external_resources.env
MOTIS_INPUT_DIR=$DATA_DIR/motis/input
MOTIS_DATA_DIR=$DATA_DIR/motis/data
MOTIS_BATCH_IN_PATH=$MOTIS_DATA_DIR/batch_in.json
STOPS_DIR=$DATA_DIR/stops
STATIONS_LIST_PATH=$STOPS_DIR/stops.csv
DIRECTORES_TO_CREATE=(
    $MOTIS_INPUT_DIR
    $MOTIS_DATA_DIR
    $MOTIS_DATA_DIR/log
    $STOPS_DIR
)

# Date and time
CURRENT_YEAR=$(date +"%Y")
TOMORROW=$(date -d 'tomorrow' +"%Y%m%d")
TOMORROW_START_TIME=$(date -d "tomorrow" +"%Y-%m-%dT03:00:00Z")

# External resources
sed -i "s/{YEAR}/$CURRENT_YEAR/g" $EXTERNAL_RESOURCES_FILE
source $EXTERNAL_RESOURCES_FILE

# Setup data paths
rm -rf "$DATA_DIR/*"
for dir in "${DIRECTORES_TO_CREATE[@]}"; do
    mkdir -p "$dir"
done
chmod -R 777 $MOTIS_DATA_DIR

# Default motis config
cat <<EOF > $MOTIS_INPUT_DIR/config.ini
modules=intermodal
modules=guesser
modules=lookup
modules=osrm
modules=ppr
modules=routing

intermodal.router=routing

dataset.begin=$TOMORROW

[import]
paths=schedule:/input/schedule
paths=osm:/input/latest.osm.pbf
EOF

# Get OSM
curl -L $OSM_LATEST -o $MOTIS_INPUT_DIR/latest.osm.pbf

# Get timetable
curl -L $TIMETABLE_LATEST -o $MOTIS_INPUT_DIR/timetable-gtfs.zip
unzip -o ./data/motis/input/timetable-gtfs.zip -d $MOTIS_INPUT_DIR/schedule
rm $MOTIS_INPUT_DIR/timetable-gtfs.zip

# Get stops list
curl -L $STOPS_LATEST -o $STOPS_DIR/stops.zip
shopt -s nullglob
rm $STOPS_DIR/*.csv
unzip -o $STOPS_DIR/stops.zip -d $STOPS_DIR
rm $STOPS_DIR/stops.zip
mv $STOPS_DIR/*.csv $STOPS_DIR/stops.csv

# Generate motis batchfile
IFS=',' read -r -a START_STATIONS_ARRAY <<< "$START_STATIONS"

stn_argument=""
for stn in "${START_STATIONS_ARRAY[@]}"; do
    stn_argument+="-s $stn "
done
stn_argument="${stn_argument% }"

python generate_batchfile.py -a $MIN_DESTINATION_ALTITUDE -t $TOMORROW_START_TIME -o $MOTIS_BATCH_IN_PATH $stn_argument

# Get snow coverage data
python get_snow_coverage.py
