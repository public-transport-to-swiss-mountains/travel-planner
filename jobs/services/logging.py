"""Logging helper classes"""
import logging

from config.config import Config


class AppLogger:
    """Application logging handler"""
    FORMATTER = '%(asctime)s [%(levelname)s] %(name)s :: %(message)s'
    DEFAULT_LEVEL = 'INFO'

    def __init__(self, name: str = __name__):
        self._logger = logging.getLogger(name)
        self.set_loglevel(Config.LOGLEVEL)

    def set_loglevel(self, level: str):
        """Set log level

        Arguments:
            level -- Level name, compatible with Logging module
        """
        if not self._logger.handlers:
            logging.basicConfig(format=self.FORMATTER, level=level)
        self._logger.setLevel(level)

    @property
    def logger(self):
        """Logger instance

        Returns:
            Logger instance
        """
        return self._logger
