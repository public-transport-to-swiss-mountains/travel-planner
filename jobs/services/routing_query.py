import json
from dataclasses import dataclass
from datetime import datetime


@dataclass
class Station:
    """Public transport stop (station)"""
    name: str
    identifier: str


def routing_query(
    start_station: Station,
    destination_station: Station,
    starts_at: datetime,
    min_connection_count: int = 3
 ) -> str:
    """Create query for MOTIS

    Arguments:
        start_station_name -- Start station name
        start_station_identifier -- Start station identifier
        destination_station_name -- Destination station name
        destination_station_identifier -- Destination station identifier
        starts_at -- Start datetime

    Keyword Arguments:
        min_connection_count -- Number of connections (default: {3})

    Returns:
        Routing query json string
    """
    start_epoch = int(starts_at.timestamp())
    query = {
        'destination': {
            'type': 'Module',
            'target': '/intermodal'
        },
        'content_type': 'IntermodalRoutingRequest',
        'content': {
            'start_type': 'PretripStart',
            'start': {
            'station': {
                'name': start_station.name,
                'id': f'Parent{start_station.identifier}'
            },
            'interval': {
                'begin': start_epoch,
                'end': start_epoch
            },
            'min_connection_count': min_connection_count,
            'extend_interval_earlier': False,
            'extend_interval_later': True
            },
            'start_modes': [],
            'destination_type': 'InputStation',
            'destination': {
                'name': destination_station.name,
                'id': f'Parent{destination_station.identifier}'
            },
            'destination_modes': [],
            'search_type': 'Default',
            'search_dir': 'Forward'
        }
    }
    return json.dumps(query, ensure_ascii=False)
