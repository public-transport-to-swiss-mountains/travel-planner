import polars as pl

from config.config import Config

CH_ISO_COUNTRYCODE = 'CH'


def station_list(
        stations_list: str = Config.STATIONS_LIST_PATH) -> pl.DataFrame:
    """Read stations list and keep only relevant attributes and records with
    valid altitude

    Keyword Arguments:
        stations_list -- Stn list CSV (default: {Config.STATIONS_LIST_PATH})

    Returns:
        Stations list (polars df)
    """
    return (
        pl.read_csv(stations_list, separator=';')
        .select([
            'number',
            'designationOfficial',
            'stopPoint',
            'isoCountryCode',
            'height',
            'wgs84East',
            'wgs84North'
        ])
        .filter(
            pl.col('stopPoint').eq(True)
            & pl.col('isoCountryCode').eq(CH_ISO_COUNTRYCODE)
        )
    )
