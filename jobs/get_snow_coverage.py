import datetime
import io

import geopandas as gpd
import polars as pl
import requests
from shapely.geometry import LineString

from config.config import Config
from services.logging import AppLogger
from services.station_list_reader import station_list

logger = AppLogger('get_snow_coverage').logger


def generate_snow_coverage_file():
    """Add snow coverage information to station list

    Returns:
        Station list with snow coverage information
    """
    stations = station_list()

    geo_line_file = generate_espg3857_line(stations)
    snow_depth = get_exolabs_snow_depth(geo_line_file)

    stations = (
        stations
        .with_columns(snow_depth=snow_depth)
        .sort('snow_depth', descending=True)
    )
    return stations


def generate_espg3857_line(stations: pl.DataFrame) -> io.BytesIO:
    """Generate ESPG:3857 line file from station list. All stations positions
    are converted from ESPG:4326 to ESPG:3857 and use to create a single
    GeoJSON line.

    Arguments:
        stations -- Stations list

    Returns:
        GeoJSON in-memory line file
    """
    line = LineString(zip(stations['wgs84East'], stations['wgs84North'], strict=False))
    gdf = (
        gpd.GeoDataFrame(geometry=[line], crs='EPSG:4326')
        .to_crs('EPSG:3857')
    )
    geo_line_file = io.BytesIO()
    gdf.to_file(geo_line_file, driver='GeoJSON')
    geo_line_file.seek(0)

    return geo_line_file


def get_exolabs_snow_depth(lines_geojson: io.BytesIO) -> pl.Series:
    """Extract snow depth information from Exolabs API

    Arguments:
        lines_geojson -- GeoJSON in-memory line file

    Returns:
        Series with snow depth in cm
    """
    res = requests.post(
        url=f'{Config.EXOLABS_BASE_URL}/{Config.EXOLABS_API_KEY}',
        files={'lines.geojson': lines_geojson},
        timeout=120
    )
    if res.status_code != 200:
        logger.exception('Error %s calling Exolabs API', res.status_code)
        return pl.lit(None)

    return pl.Series(res.json()['features'][0]['properties']['snow_depth'])


def write_snow_coverage_csv(stn_snow_coverage: pl.DataFrame):
    """Write snow coverage output to CSV file

    Arguments:
        stn_snow_coverage -- _description_
    """
    today = datetime.datetime.now(tz=datetime.UTC).date()
    (
        stn_snow_coverage
        .select([
            'number',
            'snow_depth',
        ])
        .with_columns(updated_at=pl.lit(today))
        .write_csv(Config.SNOW_COVERAGE_LIST_PATH, separator=';')
    )


if __name__ == '__main__':
    logger.info('Generating snow coverage file')
    snow_coverage = generate_snow_coverage_file()

    logger.info('Writing snow coverage to CSV')
    write_snow_coverage_csv(snow_coverage)
