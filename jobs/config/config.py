import os
from pathlib import Path


class Config:
    """General settings"""
    LOGLEVEL = os.getenv('LOGLEVEL', 'INFO')

    DATA_DIR = Path(os.getenv('DATA_DIR'))
    STATIONS_LIST_PATH = DATA_DIR / 'stops/stops.csv'
    TRAVELTIMES_LIST_PATH = DATA_DIR / 'stops/traveltimes.csv'
    SNOW_COVERAGE_LIST_PATH = DATA_DIR / 'stops/snow_coverage.csv'
    MOTIS_BATCH_IN_PATH = DATA_DIR / 'motis/data/batch_in.json'
    MOTIS_BATCH_OUT_PATH = DATA_DIR / 'motis/data/batch_out.json'

    EXOLABS_BASE_URL = os.getenv('EXOLABS_BASE_URL')
    EXOLABS_API_KEY = os.getenv('EXOLABS_API_KEY')
