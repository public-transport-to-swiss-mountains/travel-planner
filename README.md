# Public transport to Swiss Mountains - Travel planner

See the [Travel destination finder readme](https://gitlab.com/public-transport-to-swiss-mountains/destination-finder/-/blob/main/README.md) for general project information.

## Travel planner
This respository contains the core functionality of the system. The travel planner's purpose is to calculate the duration between a given starting location and all possible destinations based on specific criteria. The travel planner relies on the [MOTIS Project](https://motis-project.de/). The underlying data utilzied include OpenStreatMaps data provided by [Geofabrik GmbH](https://www.geofabrik.de/) and time tables and public transport stop lists distributed by the [Open data platform mobility Switzerland](https://opentransportdata.swiss/)

## Usage
1. Create `.env` file using `.env.example`, define custom data directory on host machine if desired.
1. Generate schedules for the next day for destinations above a given altitude: `./generate_schedules.sh <altitude_masl>`

## Development
### Job scripts
Execute init script

```
cd jobs
cp .env.example .env    # Adapt file content according to personal dev environment
pip install -r requirements.txt
(export $(grep -v '^#' .env | xargs) && bash init.sh)
```

Execute single Python script
```
cd jobs
cp .env.example .env    # Adapt file content according to personal dev environment
pip install -r requirements.txt
pip install "python-dotenv[cli]"
dotenv run python my_script.py
```

## License
See [license file](https://gitlab.com/public-transport-to-swiss-mountains/travel-planner/-/blob/main/LICENSE?ref_type=heads).
