#!/bin/bash

# Check if the altitude argument is provided
if [ -z "$1" ]; then
    echo "Error: Altitude argument is missing."
    exit 1
fi

# Run Docker Compose commands
docker compose run --build --env MIN_DESTINATION_ALTITUDE=$1 jobs
docker compose run motis
docker compose run jobs python read_schedules.py
